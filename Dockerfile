FROM node AS builder
WORKDIR /app
COPY . ./
RUN yarn install 
RUN yarn build

# 安装nginx
FROM nginx
COPY --from=builder /app/build /usr/share/nginx/html

